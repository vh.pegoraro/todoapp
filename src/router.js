import VueRouter from "vue-router"

import TasksIndex from "./pages/tasks/index.vue"
import TasksForm from "./pages/tasks/form.vue"

const tasksIndex = { path: "/tasks", component: TasksIndex }
const tasksForm = { path: "/tasks/:id/form", component: TasksForm }
const notFound = { path: "*", component: TasksIndex }

const router = new VueRouter({
  routes: [tasksIndex, tasksForm, notFound],
  mode: "history",
})

export default router
