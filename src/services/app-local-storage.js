export default class AppLocalStorage {

  static check() {

    const todoApp = { tasks: [] }

    if (!localStorage.todoApp)
      localStorage.setItem("todoApp", JSON.stringify(todoApp))
  }

  static get(prop) {

    AppLocalStorage.check()

    const todoApp = JSON.parse(localStorage.todoApp)

    return todoApp[prop]
  }

  static set(prop, value) {

    AppLocalStorage.check()

    var todoApp = JSON.parse(localStorage.todoApp)

    todoApp[prop] = value

    localStorage.setItem("todoApp", JSON.stringify(todoApp))
  }
}
