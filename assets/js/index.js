$(document).ready(function() {

  loadTasks()
})

function loadTasks() {

  var tasks = appLocalStorage.get("tasks")

  $("#tasks-table tbody").html("")

  $(tasks).each(function(index, task) {

    addTask(task)
  })

  if (tasks.length > 0)
    $(".todos-container").removeClass("hidden")

}

function addTask(task) {

  var row = ''
  var navTo = "utils.navTo('form.html?id=" + task.id + "')"

  row += '<tr class="clickable" onclick="' + navTo + '">'
  row += '  <td>' + task.id + '</td>'
  row += '  <td>' + getStatusDescription(task.status) + '</td>'
  row += '  <td>' + task.description + '</td>'
  row += '  <td>' + (task.deadline ? task.deadline : "" ) + '</td>'
  row += '</tr>'

  $("#tasks-table tbody").append(row)
}

function getStatusDescription(status) {

  var description = ""

  if (status == 1) {
    description = "Pendente"
  } else if (status == 2) {
    description = "Em andamento"
  } else if (status == 3) {
    description = "Concluída"
  } else if (status == 4) {
    description = "Cancelada"
  }

  return description
}

function createTasks() {

  var tasks = [
    { id: 1, description: "Tomar uma cerveja", status: 1, deadline: "10/03/17"},
    { id: 2, description: "Ver futebol", status: 4 },
    { id: 3, description: "Estudar HTML", status: 2, deadline: "06/03/17"},
    { id: 4, description: "Responder e-mails", status: 3, deadline: "05/03/17"},
  ]

  appLocalStorage.set("tasks", tasks)

  loadTasks()
}
