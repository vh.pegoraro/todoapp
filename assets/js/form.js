var _isCreating = false
var _isUpdating = false

$(document).ready(function() {

  $("#btn-confirm").click(confirm)

  loadData()
})

function loadData() {

  var task = {}
  var id = utils.getUrlParameter("id")

  if (id) {

    _isUpdating = true

    $("#label-update").removeClass("hidden")
    $("#column-id").removeClass("hidden")
    $("#column-btn-delete").removeClass("hidden")

    task = getTaskById(id)

  } else {

    _isCreating = true

    $("#label-create").removeClass("hidden")
  }

  if (_isUpdating && !task) {
    utils.navTo("not-found.html")
    return
  }

  fillForm(task)
}

function getTaskById(id) {

  var task = null
  var todoApp = JSON.parse(localStorage.todoApp)
  var tasks = todoApp.tasks

  id = parseInt(id)

  $(tasks).each(function(index, t) {

    if (t.id === id)
      task = t
  })

  return task
}

function fillForm(task) {

  $("#task-id").val(task.id)
  $("#task-description").val(task.description)
  $("#task-status").val(task.status)
  $("#task-deadline").val(task.deadline)
}

function confirm() {

  var task = {
    id: _isCreating ? getNextId() : $("#task-id").val(),
    description: $("#task-description").val(),
    status: $("#task-status").val(),
    deadline: $("#task-deadline").val(),
  }

  var tasks = appLocalStorage.get("tasks")

  task.id = parseInt(task.id)

  if (_isCreating) {

    tasks.push(task)

  } else {

    $(tasks).each(function(index, t) {

      if (t.id === task.id) {
        tasks[index] = task
      }
    })
  }

  appLocalStorage.set("tasks", tasks)

  utils.navTo("index.html")
}

function getNextId() {

  var maxId = 0
  var nextId = 0
  var tasks = appLocalStorage.get("tasks")

  $(tasks).each(function(index, task) {

    if (task.id > maxId)
      maxId = task.id
  })

  nextId = (maxId + 1)

  return nextId
}
