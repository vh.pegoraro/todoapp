var appLocalStorage = {}

appLocalStorage.check = function() {

  var todoApp = { tasks: [] }

  if (!localStorage.todoApp)
    localStorage.setItem("todoApp", JSON.stringify(todoApp))
}

appLocalStorage.get = function(prop) {

  appLocalStorage.check()

  var todoApp = JSON.parse(localStorage.todoApp)

  return todoApp[prop]
}

appLocalStorage.set = function(prop, value) {

  appLocalStorage.check()

  var todoApp = JSON.parse(localStorage.todoApp)

  todoApp[prop] = value

  localStorage.setItem("todoApp", JSON.stringify(todoApp))
}
