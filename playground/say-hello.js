const showAge = (age) => {

  console.log("Pronto! Sua idade é: " + age)
}

const doSomethingAsync = (data) => {

  console.log("Seu nome é: " + data.name)

  console.log("Processando ...")

  setTimeout(() => {

    showAge(data.age)

    data.onFinish()
  }, 2000)

  console.log("Seu sobrenome é: " + data.lastName)
}

doSomethingAsync({
  name: "Victor Hugo",
  lastName: "Pegoraro Pereira",
  age: 32,
  onFinish: () => {

    console.log("Porra !!!! Agora foi !!! =D")
  }
})
